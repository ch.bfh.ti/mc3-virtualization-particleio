# Particle.io
[Link Website](https://www.particle.io/)

``` C
int i = 0;

void handler(const char *topic, const char *data) {
    Serial.println("received " + String(topic) + ": " + String(data));
}

void setup() {
    Serial.begin(115200);
    Particle.subscribe("particle/device/name", handler);
    Particle.syncTime();
    waitUntil(Particle.syncTimeDone);
    Particle.publish("custom-message-start", Time.timeStr(), PUBLIC);
}

void loop() {
    ++i;
    Particle.publish("custom-message-count", String(i), PUBLIC);
    if (i % 4 == 0){
        Particle.publish("azure-count", String(i), PRIVATE);
    }
    delay(15 * 1000);
}
```