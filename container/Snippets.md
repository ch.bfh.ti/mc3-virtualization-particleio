# Docker Snippets

## Bulding a image

```shell
docker build --tag mydockerimage:latest .
```

## List images

```shell
docker image ls
```

## Run a container

```shell
docker run \
    --name mywebserver2 \
    -p 80:80 \
    -v $(pwd)/html:/usr/share/nginx/html:ro \
    mydockerimage:latest
```

<http://localhost/>

## Describe container in a Compose-File

```yaml
version: "3.7"

services:
  my-docker-container:
    image: mydockerimage:latest
    ports:
      - target: 80
        published: 80
        protocol: tcp
        mode: host
    volumes:
      - type: bind
        source: ./html
        target: /usr/share/nginx/html
        read_only: true
```

### Fire up a container by Docker-Compose

```shell
docker-compose up [--file path-to/docker-compose.yaml]
```

## Create a service (swarm mode)

```shell
docker service create \
    --name myservice \
    --publish published=80,target=80 \
    --mount type=bind,source=$(pwd)/html,destination=/usr/share/nginx/html \
    mydockerimage:latest
```

## Deploy a service with stack files (swarm mode)

```yaml
version: "3.7"

services:
  my-docker-stack:
    image: mydockerimage:latest
    ports:
      - target: 80
        published: 80
        protocol: tcp
        mode: ingress
    volumes:
      - type: bind
        source: ./html
        target: /usr/share/nginx/html
        read_only: true
    deploy:
      mode: replicated
      replicas: 1
```

```shell
docker stack deploy --compose-file stack-file.yaml stack-demo
```

## List services

```shell
docker service ls
```

## Show logs in Docker Service

```shell
docker service logs -f stack-demo_my-docker-stack
```

<http://localhost/>

## Inspect the service

```shell
docker service inspect stack-demo_my-docker-stack
```

## Scale instances

### up

```shell
docker service scale stack-demo_my-docker-stack=50
docker service ps stack-demo_my-docker-stack
```

### down

```shell
docker service scale stack-demo_my-docker-stack=5 --detach
watch -n 1 docker service ps stack-demo_my-docker-stack
```

## Clean up things

```shell
docker service rm $(docker service ls -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)
```

[Backup to Repo-README](../README.md)
