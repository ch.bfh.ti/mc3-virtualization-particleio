# Presentation of Virtualizations and Particle.io-Overview

## Slide Deck

* [Link to PDF version](https://1drv.ms/b/s!Aj01okbcdA_Dgod2dwr1NHd7LRz0Xw)
* [Link to PPTX version](https://1drv.ms/p/s!Aj01okbcdA_DgodGYmnITvG7e3eZ9Q)

## Docker files

### Snippet mit Commands

* [Markdown Snippet File](/container/Snippets.md)

### "Infrastructure as a Code" Examples

* [Dockerfile example](https://github.com/nginxinc/docker-nginx/blob/master/stable/stretch-perl/Dockerfile) (ⓒ NGINX Inc.)
* [Docker Compose-File example](/container/docker-compose.yaml)
* [Docker Stack-File example](/container/stack-file.yaml)

## Particle.io

* [Markdown Code](/particleio/Info.md)